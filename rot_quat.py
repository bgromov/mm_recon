#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 20 01:26:12 2017

@author: denis
"""
#%%============================================================================
# Alessandro
#==============================================================================
import numpy as np
import quaternion
import math

# measured from myo: pointing horizontally towards wall
a=np.quaternion(0.96,-0.08,0,0.23) # wxyz

# measured from myo: pointing horizontally towards another wall (90 deg angle)
b=np.quaternion(0.8,-0.16,0.09,-0.57) # wxyz

# find the quaternion representing the rotation between the two
r=b * a.inverse() # b = r * a --> r = b * a**-1
r_vec=quaternion.as_rotation_vector(r)
angle=np.linalg.norm(r_vec)
axis=r_vec/angle

print("Angle: {} pi".format(angle/math.pi))
print("Axis: {}".format(axis))

# May be the correct way to rotate a given quaternion around an axis

alpha = np.deg2rad(-90)
axis = np.array([0.0,0.0,1.0])

r1=np.quaternion(np.cos(alpha/2), axis[0]*np.sin(alpha/2), axis[1]*np.sin(alpha/2), axis[2]*np.sin(alpha/2))
r2=quaternion.from_rotation_vector(axis*alpha)
assert(r1==r2)

ar = (r1 * a).normalized()

ar    

#%%============================================================================
# Boris
#==============================================================================

import PyKDL as kdl
rot1 = kdl.Rotation.Quaternion(*[-0.08,0,0.23,0.96])
rot1.DoRotZ(-(math.pi/2))
rot1.GetQuaternion()