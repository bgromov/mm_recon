#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 14 14:52:01 2017

@author: denis
"""
from __future__ import absolute_import, division, print_function

import numpy as np
import rosbag_pandas
import pandas as pd

clf = joblib.load('/home/denis/filename.pkl')
d = collections.deque(maxlen=200)

for i in xrange(1000000):
    d.append([i,i,i])
    n = pd.DataFrame(np.array(d))
    row = np.hstack((n.mean().values, n.std().values))
    print(row)