# %%
from __future__ import absolute_import, division, print_function

import numpy as np
import rosbag_pandas
import pandas as pd
import matplotlib.pyplot as plt
from tqdm import tqdm

bags_rf = np.array([["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/rf/test_02.bag",0.2],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/rf/test_04.bag",0.4],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/rf/test_05.bag",0.5],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/rf/test_06.bag",0.6],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/rf/test_075.bag",0.75],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/rf/test_08.bag",0.8],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/rf/test_085.bag",0.85],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/rf/test_09.bag",0.9]])


bags_nn = np.array([["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/nn/test_02.bag",0.2],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/nn/test_04.bag",0.4],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/nn/test_05.bag",0.5],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/nn/test_06.bag",0.6],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/nn/test_075.bag",0.75],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/nn/test_08.bag",0.8],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/nn/test_085.bag",0.85],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/nn/test_09.bag",0.9],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/nn/test_097.bag",0.97]])

def make_plot(bags):
    hits = {}
    # Sounds and proba are the same for every bag file
    gestures_proba = rosbag_pandas.bag_to_dataframe(bags[0][0],include=["/gesture_1_proba"])
    sounds = rosbag_pandas.bag_to_dataframe(bags[0][0],include=["/robotsound"])
    
    t0_proba = rosbag_pandas.bag_to_dataframe(bags[0][0],include=["/robotsound"]).index[0]
    
    gestures_proba = gestures_proba.set_index((gestures_proba.index - t0_proba).total_seconds())
    sounds = sounds.set_index((sounds.index-t0_proba).total_seconds())
    
    # Plot ax
    fig,(ax,ax2) = plt.subplots(nrows=2,sharex=True,figsize=(15, 6)) 
    ax.set_ylim([0, 1])
    #gestures_proba.plot(kind='line', label='line', ax=ax, color='red')
    ax.plot(gestures_proba)
    
    for bag in tqdm(bags):
        t0 = rosbag_pandas.bag_to_dataframe(bag[0],include=["/robotsound"]).index[0]
        gestures = rosbag_pandas.bag_to_dataframe(bag[0],include=["/gesture_1_debounced"])
        gestures = gestures.set_index((gestures.index-t0).total_seconds())
        #deltas.append(gestures.index[0])
        hits[bag[1]]=gestures[gestures["gesture_1_debounced__data"]==1].index
        
    # ..and plot them once
    for treshold,thits in hits.items():
#==============================================================================
#         ax2.plot(thits,
#                  [float(treshold)]*len(thits),
#                  'bo')
#==============================================================================
        tp = []
        fp = []
        for hit in thits:
            df = sounds[hit-3:hit]
            if len(df) > 0:
                tp.append(hit)
            else:
                fp.append(hit)
                   
        ax2.plot(pd.core.indexes.numeric.Float64Index(tp),
                 [float(treshold)]*len(tp),
                 'bo')
        ax2.plot(pd.core.indexes.numeric.Float64Index(fp),
                 [float(treshold)]*len(fp),
                 'ro')
        ax.axhline(y=treshold,c="blue",linewidth=0.5,alpha=0.5)
        ax2.axhline(y=treshold,c="blue",linewidth=0.5,alpha=0.5) 
    
    ax.set_title("Hits variation changing treshold")
    ax.set_ylim([0,1])
    ax2.set_ylim([0,1])
    #ax.set_xlim([-0.12*10**11,10**11])
    
    idx = pd.Series(sounds.index[:-1])
    ax.vlines(x=idx, ymin=0, ymax=1,linewidth=5, alpha=0.2, color="g")


make_plot(bags_rf)
make_plot(bags_nn)
#%%
# Stats
def roc_table(bags):
    roc_table = []
    for bag,treshold in bags:
        print(bag,treshold)
        sounds = rosbag_pandas.bag_to_dataframe(bag,include=["/robotsound"])
        gestures = rosbag_pandas.bag_to_dataframe(bag,include=["/gesture_1_debounced"])
        tp = 0
        fp = 0
        fn = 0
        g_temp = gestures
        for t in sounds.index[:-1]:
            d = g_temp[t:(t + pd.Timedelta(seconds=3))]
            if(len(d)==1):
                tp+=1
                g_temp=g_temp.drop(d.index.values)
            else:
                if(len(d)==0):
                    fn+=1
                else:
                    fn+=(len(d)-1)
                    
        fp+=len(g_temp)
        roc_table.append([treshold,tp,fp,fn])
    return np.array(roc_table)

def plot_roc_table(roc_table,title):
    fig = plt.figure()  # create a figure object
    ax = fig.add_subplot(1, 1, 1)  # create an axes object in the figure
    z = roc_table[:,2].astype(float)/103*60
    y = roc_table[:,1].astype(float)/10
    ax.plot(z,y,"bo",zorder=2)
    
    for i, txt in enumerate(roc_table[:,0]):
        ax.annotate(txt, (z[i],y[i]),xytext=(z[i]+0.02,y[i]+0.02))
        
    ax.set_ylim(bottom=0)
    ax.set_xlim([0,6])
    ax.set_ylabel('True positive %')
    ax.set_xlabel('False Positive per minute')       
    ax.set_title(title)
            
        
plot_roc_table(roc_table(bags_rf),'Random Forest results on test bag')
plot_roc_table(roc_table(bags_nn),'Neural Network results on test bag') 

#==============================================================================
# y=[2.56422, 3.77284,3.52623,3.51468,3.02199]
# z=[0.15, 0.3, 0.45, 0.6, 0.75]
# n=[58,651,393,203,123]
# 
# fig, ax = plt.subplots()
# ax.scatter(z, y)
# 
# for i, txt in enumerate(n):
#     ax.annotate(txt, (z[i],y[i]))
#==============================================================================
