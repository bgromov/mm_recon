# %%
from __future__ import absolute_import, division, print_function

import numpy as np
import rosbag_pandas
import pandas as pd
import matplotlib.pyplot as plt
from tqdm import tqdm
import sys
sys.path.append('/home/denis/catkin_ws/src/mm_recon/src/mm_recon')
import mm_plt

#%%
def bagfile(filename):
    return  "/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/myo/"+filename+".bag"

bags2 = np.array([[bagfile('2win/myo_05'),0.5],
                 [bagfile('2win/myo_06'),0.6],
                 [bagfile('2win/myo_07'),0.7],
                 [bagfile('2win/myo_075'),0.75],
                 [bagfile('2win/myo_08'),0.8]])
    
bags3 = np.array([[bagfile('3win/myo_05'),0.5],
                 [bagfile('3win/myo_085'),0.85],
                 [bagfile('3win/myo_075'),0.75],
                 [bagfile('3win/myo_09'),0.9]])

bags25 = np.array([[bagfile('25win/myo_05'),0.5]])
onesens = np.array([[bagfile('1sens/myo_1sens_02'),0.2],
                    [bagfile('1sens/myo_1sens_05'),0.5]])
    
bags_rf = np.array([["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/rf/test_02.bag",0.2],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/rf/test_04.bag",0.4],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/rf/test_05.bag",0.5],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/rf/test_06.bag",0.6],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/rf/test_075.bag",0.75],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/rf/test_08.bag",0.8],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/rf/test_085.bag",0.85],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/rf/test_09.bag",0.9]])


bags_nn = np.array([["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/nn/test_02.bag",0.2],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/nn/test_04.bag",0.4],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/nn/test_05.bag",0.5],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/nn/test_06.bag",0.6],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/nn/test_075.bag",0.75],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/nn/test_08.bag",0.8],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/nn/test_085.bag",0.85],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/nn/test_09.bag",0.9],
        ["/home/denis/catkin_ws/src/mm_recon/bags/plot_treshold_bags/nn/test_097.bag",0.97]])
    
bags_no_aug = np.array([[bagfile('no_data_aug/nodataaug'),0.5]])
    
#==============================================================================
# bags3 = np.array([[bagfile('3win/myo_075'),0.75],
#                   [bagfile('3win/myo_05'),0.5],
#                   [bagfile('3win/myo_085'),0.85],
#                   [bagfile('3win/myo_09'),0.9]])
# 
#==============================================================================

def make_plot(bags,title,topic_proba="pointing_proba",topic_debounced="pointing_debounced",xbound=200):
    hits = {}
    # Sounds and proba are the same for every bag file
    gestures_proba = rosbag_pandas.bag_to_dataframe(bags[0][0],include=['/'+topic_proba])
    sounds = rosbag_pandas.bag_to_dataframe(bags[0][0],include=["/robotsound"])
    t0_proba = rosbag_pandas.bag_to_dataframe(bags[0][0],include=["/robotsound"]).index[0]
    
    gestures_proba = gestures_proba.set_index((gestures_proba.index - t0_proba).total_seconds())
    sounds = sounds.set_index((sounds.index-t0_proba).total_seconds())
    
    # Plot ax sharex=True,
    fig,(ax,ax2) = plt.subplots(nrows=2,figsize=(15, 6)) 
    ax.set_ylim([0, 1])
    #gestures_proba.plot(kind='line', label='line', ax=ax, color='red')
    ax.plot(gestures_proba,label='/gesture_proba')
    
    for bag in tqdm(bags):
        t0 = rosbag_pandas.bag_to_dataframe(bag[0],include=["/robotsound"]).index[0]
        print(t0)
        gestures = rosbag_pandas.bag_to_dataframe(bag[0],include='/'+topic_debounced)
        gestures = gestures.set_index((gestures.index-t0).total_seconds())
        hits[bag[1]]=gestures[gestures[topic_debounced+"__data"]==1].index
        
    # ..and plot them once
    for treshold,thits in hits.items():
        #==============================================================================
        #         
        #==============================================================================
        tp = []
        fp = []
        sounds_missed = pd.DataFrame(sounds)
        for hit in thits:
            df = sounds[hit-3:hit]
            if len(df) > 0:
                tp.append(hit)
                try:
                    sounds_missed = sounds_missed.drop(df.index.values)
                except ValueError:
                    pass
            else:
                fp.append(hit)
               
        tp_points = ax2.plot(pd.core.indexes.numeric.Float64Index(tp),
                 [float(treshold)]*len(tp),
                 'go')
        fp_points = ax2.plot(pd.core.indexes.numeric.Float64Index(fp),
                 [float(treshold)]*len(fp),
                 'rx')
        fn_points = ax2.plot(pd.Series(sounds_missed.index[:-1]),
                 [float(treshold)]*(len(sounds_missed)-1),
                 'bo',mfc='none',mec='b')
        ax.axhline(y=treshold,c="blue",linewidth=0.5,alpha=0.5)
        ax2.axhline(y=treshold,c="blue",linewidth=0.5,alpha=0.5)
        
    
    ax.set_title(title,fontdict=mm_plt.title_font)
    ax.set_ylim([0,1])
    ax2.set_ylim([0,1])
    ax.set_xlim([-10,xbound])
    ax2.set_xlim([-10,xbound])
    
    ax2.plot([],'go',label='TP')
    ax2.plot([],'rx',label='FP')
    ax2.plot([],'bo',label='FN',mfc='none',mec='b')
    
    idx = pd.Series(sounds.index[:-1])
    ax.vlines(x=idx, ymin=0, ymax=1,linewidth=5, alpha=0.2, color="g",label='e0')
    #ax.legend(loc='best', bbox_to_anchor=(1, 0.7))
    #ax2.legend(loc='best', bbox_to_anchor=(1, 0.7))
    ax.legend()
    ax2.legend()
    ax.set_xticks(np.arange(0,xbound+10,10))
    ax2.set_xticks(np.arange(0,xbound+10,10))
    
    ax.set_ylabel('Threshold to hit',fontdict=mm_plt.axis_font)
    ax.set_xlabel('Time [s]',fontdict=mm_plt.axis_font) 

make_plot(bags_rf,'Stop Gesture - Random Forest','gesture_1_proba',"gesture_1_debounced",100)
make_plot(bags_nn,'Stop Gesture - NN','gesture_1_proba',"gesture_1_debounced",100)
make_plot(bags2,'Pointing Gesture - NN - 2 seconds windows')
make_plot(bags25,'Pointing Gesture - NN - 2.5 seconds windows')
make_plot(bags3,'Pointing Gesture - NN - 3 seconds windows')
make_plot(onesens,'Pointing Gesture - NN - 2.5 seconds windows - One Sensor')
make_plot(bags_no_aug,'Pointing gesture - NN - Without data augmentation')
#%%
# Stats
def roc_table(bags,topic_debounced="pointing_debounced"):
    roc_table = []
    for bag,treshold in tqdm(bags):
        sounds = rosbag_pandas.bag_to_dataframe(bag,include=["/robotsound"])
        gestures = rosbag_pandas.bag_to_dataframe(bag,include=['/'+topic_debounced])
        tp = 0
        fp = 0
        fn = 0
        g_temp = gestures
        for t in sounds.index[:-1]:
            d = g_temp[t:(t + pd.Timedelta(seconds=3))]
            if(len(d)==1):
                tp+=1
                g_temp=g_temp.drop(d.index.values)
            else:
                if(len(d)==0):
                    fn+=1
                else:
                    fn+=(len(d)-1)
                    
        fp+=len(g_temp)
        roc_table.append([treshold,tp,fp,fn])
    return np.array(roc_table)

def plot_roc_table(roc_table,title,seconds=200,sounds=19):
    fig = plt.figure()  # create a figure object
    ax = fig.add_subplot(1, 1, 1)  # create an axes object in the figure
    z = roc_table[:,2].astype(float)/seconds*60
    y = roc_table[:,1].astype(float)/sounds
    ax.plot(z,y,"bo",zorder=2)
    
    for i, txt in enumerate(roc_table[:,0]):
        ax.annotate(txt, (z[i],y[i]),xytext=(z[i]+0.02,y[i]+0.02))
        
    ax.axhline(y=1,c="blue",linewidth=0.5,alpha=0.5)   
    ax.set_ylim([0,1.07])
    ax.set_xlim([0,10])
    ax.set_ylabel('True positive %',fontdict=mm_plt.axis_font)
    ax.set_xlabel('False Positive per minute',fontdict=mm_plt.axis_font)       
    ax.set_title(title,fontdict=mm_plt.little_title_font)
            

plot_roc_table(roc_table(bags_rf,"gesture_1_debounced"),'Treshold Performances - Stop Gesture - Random Forest',103,10)
plot_roc_table(roc_table(bags_nn,"gesture_1_debounced"),'Treshold Performances - Stop Gesture - NN',103,10)
plot_roc_table(roc_table(bags2),'Treshold Performances - Pointing Gesture - 2 seconds window')
plot_roc_table(roc_table(bags25),'Treshold Performances - Pointing Gesture - 2.5 seconds window')
plot_roc_table(roc_table(bags3),'Treshold Performances - Pointing Gesture - 3 seconds window')
plot_roc_table(roc_table(onesens),'Treshold Performances - Pointing Gesture - One sensor')
plot_roc_table(roc_table(bags_no_aug),'Treshold Performances - Pointing Gesture - Without data augmentation')


#==============================================================================
# y=[2.56422, 3.77284,3.52623,3.51468,3.02199]
# z=[0.15, 0.3, 0.45, 0.6, 0.75]
# n=[58,651,393,203,123]
# 
# fig, ax = plt.subplots()
# ax.scatter(z, y)
# 
# for i, txt in enumerate(n):
#     ax.annotate(txt, (z[i],y[i]))
#==============================================================================
