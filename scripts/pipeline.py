# %%
from __future__ import absolute_import, division, print_function

import numpy as np
import rosbag_pandas
import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn import tree
from sklearn import ensemble
from sklearn import preprocessing
from sklearn.pipeline import make_pipeline
import operator
from sklearn.externals import joblib
from sklearn import metrics

from mm_recon import features_tool
import os.path
#%%

#==============================================================================
# PARAMETERS + GLOBAL VARS
#==============================================================================

bags_g1 = ["/home/denis/catkin_ws/src/mm_recon/bags/training/gesture1_fp.bag",
        "/home/denis/catkin_ws/src/mm_recon/bags/training/gesture11.bag",
        "/home/denis/catkin_ws/src/mm_recon/bags/training/gesture111.bag",
        "/home/denis/catkin_ws/src/mm_recon/bags/training/gesture11111.bag",
        "/home/denis/catkin_ws/src/mm_recon/bags/training/gesture1111.bag"] 
g1_tr = bags_g1[:-1]
g1_te = bags_g1[-1:]

columns=['imu_data__orientation_x', 
         'imu_data__orientation_y',
         'imu_data__orientation_z',
         'imu_data__orientation_w',
         'imu_data__linear_acceleration_x',
         'imu_data__linear_acceleration_y',
         'imu_data__linear_acceleration_z']

def make_ppl(clf):
    return make_pipeline(
        preprocessing.StandardScaler(),
        preprocessing.PolynomialFeatures(1),
        clf)
    #return clf

clfs = [make_ppl(ensemble.RandomForestClassifier(n_estimators=15)),
        make_ppl(tree.DecisionTreeClassifier())]

clfs_scores = {}

window_seconds = 2

final_tr_dataset = np.array([[0 for i in xrange(features_tool.n_columns(number_of_sensors=len(columns)))]])
final_te_dataset = np.array([[0 for i in xrange(features_tool.n_columns(number_of_sensors=len(columns)))]])

#==============================================================================
# Create a big, unique dataset using multiple bag files
#==============================================================================
def stats(X_tr, X_te, y_tr, y_te):
    print ("====================================")
    print ("Sizes===============================")
    print ("X_tr size:\t%d" % len(X_tr))
    print ("X_te size:\t%d" % len(X_te))
    print ("y_tr size:\t%d" % len(y_tr))
    print ("y_tr size:\t%d" % len(y_te))
    print ("====================================")
    print ("Classes=============================")
    print ("y_tr:")
    unique, counts = np.unique(y_tr, return_counts=True)
    print (np.asarray((unique, counts)).T)
    print ("y_te:")
    unique, counts = np.unique(y_te, return_counts=True)
    print (np.asarray((unique, counts)).T)
    print ("====================================")
    print ("Sensor_infos========================")
    for column in columns:
        print (column)

def process_bag_file(bagfile,gesture,destination):
    
    global final_tr_dataset
    global final_te_dataset
    
    if os.path.isfile(bagfile) :
        print("Processing %s ..." % bagfile)
    
    imudf = rosbag_pandas.bag_to_dataframe(bagfile, include=["/imu/data"])
    snddf = rosbag_pandas.bag_to_dataframe(bagfile, include=["/robotsound"])
    
    features=[]
    
    # Label windows near the sound with gesture label

    for sound_event in snddf.index[:-1]: # the last sound represents the end-session-notification
        d = pd.DataFrame()
        for t_past in range(500,0,-10): # milliseconds before the soudn has happened. A gesture has duration of avg 1 s
            t = sound_event-pd.Timedelta(milliseconds=t_past)
            d = imudf[t:(t + pd.Timedelta(seconds=window_seconds))][columns]
            # row = np.hstack((d.mean().values, d.std().values))
            row = features_tool.extract_features(d)
            features.append(np.concatenate((row, np.array([gesture])), axis=0))
        
        imudf=imudf.drop(d.index.values)
    
    # Label remaining windows with 0
    for start in range(0,200,10):
        cnt=start
        edge=0
        window=[]
        while cnt<len(imudf):
            if (cnt+window_seconds*100)<len(imudf):
                edge=cnt+window_seconds*100
                window=imudf.iloc[cnt:edge].loc[:,columns]
                # row = np.hstack((window.mean().values, window.std().values))
                row = features_tool.extract_features(window)
                features.append(np.concatenate((row, np.array([0])), axis=0))
            cnt=cnt+window_seconds*100        
        
        dataset=np.array(features)
        yolo=np.vstack(dataset)
    
    if(destination=="tr"):
        final_tr_dataset = np.vstack([final_tr_dataset,yolo])
    else:
        final_te_dataset = np.vstack([final_te_dataset,yolo])
    
def train(clfs,tr_dataset,te_dataset):
    # training_line = int(len(dataset)*0.7) 
    X_tr = tr_dataset[:,:-1]
    X_te = te_dataset[:,:-1]
    y_tr = tr_dataset[:,-1:]
    y_te = te_dataset[:,-1:]
    stats(X_tr, X_te, y_tr, y_te)
    for clf in clfs:
        clf=clf.fit(X_tr,y_tr)
        y_score=clf.predict_proba(X_te)[:,1]
        y_pred=y_score>0.5
        accuracy=accuracy_score(y_te,y_pred)
        fpr, tpr, _ = metrics.roc_curve(y_te, y_score)
        area_under_curve = metrics.roc_auc_score(y_te, y_score)
        print("=======================================================")
        print(str(clf.steps[-1][1])+"\nAccuracy:\t"+str(accuracy)+"\nAUC:\t"+str(area_under_curve))
        print("=======================================================")
        clfs_scores[clf] = area_under_curve
        
#==============================================================================
# Perform pipeline for each classifier
#==============================================================================

for bag in g1_tr:
    process_bag_file(bag,1,"tr")
    
for bag in g1_te:
    process_bag_file(bag,1,"te")
    
# np.random.shuffle(final_dataset)

train(clfs,final_tr_dataset,final_te_dataset)

# Compare every classifier, sort them by accuracy

sorted_clfs_dict = sorted(clfs_scores.items(), key=operator.itemgetter(1))

# Serialize the best one that will be used in rosnode

joblib.dump(sorted_clfs_dict[1][0], '/home/denis/catkin_ws/src/mm_recon/cls/best_classifier_1_1.pkl') 

#%%
#==============================================================================
# FEATURES COMPARISON
# Create classifiers based on different features, but always using the three-window approach
#==============================================================================
def reshape(four_dim_array,combo,labels):
    n, w, f, s = four_dim_array.shape
    obs = four_dim_array[:,:,combo, ...].reshape(n, -1)
    print(labels.shape)
    print(obs.shape)
    partial_dataset = np.concatenate([obs, labels], axis=1)
    return partial_dataset

number_of_features=len(features_tool.features_used)
shapeable_tr_dataset = final_tr_dataset[:,:-1]
shapeable_tr_dataset = shapeable_tr_dataset.reshape((len(shapeable_tr_dataset),
                                              features_tool.sub_windows,#3
                                              len(features_tool.features_used),
                                              len(columns)
                                              ))
shapeable_te_dataset = final_te_dataset[:,:-1]
shapeable_te_dataset = shapeable_te_dataset.reshape((len(shapeable_te_dataset),
                                              features_tool.sub_windows,#3
                                              len(features_tool.features_used),
                                              len(columns)
                                              ))

labels_tr = final_tr_dataset[:,-1:]
labels_te = final_te_dataset[:,-1:]

plot_data_3sub = {}
for combo in features_tool.combo_features(xrange(number_of_features)):
    print("========================================================")
    print("Using features -->"+str(combo))
    print("========================================================")
    sub_tr = reshape(shapeable_tr_dataset,combo,labels_tr)
    sub_te = reshape(shapeable_te_dataset,combo,labels_te)
    train(clfs,sub_tr,sub_te)
    plot_data_3sub[combo] = clfs_scores.values()

#==============================================================================
# Same for the one-window approach
#==============================================================================
shapeable_tr_dataset = final_tr_dataset[:,-(len(features_tool.features_used)*len(columns)+1):-1]
shapeable_tr_dataset = shapeable_tr_dataset.reshape((len(shapeable_tr_dataset),
                                              1,#3
                                              len(features_tool.features_used),
                                              len(columns)
                                              ))
shapeable_te_dataset = final_te_dataset[:,-(len(features_tool.features_used)*len(columns)+1):-1]
shapeable_te_dataset = shapeable_te_dataset.reshape((len(shapeable_te_dataset),
                                              1,#3
                                              len(features_tool.features_used),
                                              len(columns)
                                              ))

labels_tr = final_tr_dataset[:,-1:]
labels_te = final_te_dataset[:,-1:]
 
plot_data_nosub = {}
for combo in features_tool.combo_features(xrange(number_of_features)):
    print("========================================================")
    print("Using features -->"+str(combo))
    print("========================================================")
    sub_tr = reshape(shapeable_tr_dataset,combo,labels_tr)
    sub_te = reshape(shapeable_te_dataset,combo,labels_te)
    train(clfs,sub_tr,sub_te)
    plot_data_nosub[combo] = clfs_scores.values()

#==============================================================================
# PLOT RESULTS
# Extract results from dictionaries
#==============================================================================
import collections

sorted_plot_3sub = collections.OrderedDict(sorted(plot_data_3sub.items(), key=lambda t: len(t[0])))
for key, value in plot_data_3sub.items():
    plot_data_3sub[key] = value
#==============================================================================
# Sort classifier results by features 
#==============================================================================
sorted_plot_nosub = collections.OrderedDict(sorted(plot_data_nosub.items(), key=lambda t: len(t[0])))
for key, value in plot_data_nosub.items():
    sorted_plot_nosub[key] = value

x_3sub_dt = np.arange(len(sorted_plot_3sub))
y_3sub_dt = np.array(sorted_plot_3sub.values())[:,1]

x_nosub_dt = np.arange(len(sorted_plot_nosub))
y_nosub_dt = np.array(sorted_plot_nosub.values())[:,1]

x_3sub_rf = np.arange(len(sorted_plot_3sub))
y_3sub_rf = np.array(sorted_plot_3sub.values())[:,0]

x_nosub_rf = np.arange(len(sorted_plot_nosub))
y_nosub_rf = np.array(sorted_plot_nosub.values())[:,0]
#%%
#==============================================================================
# Create comparison histograms
#==============================================================================
import matplotlib.pyplot as plt
import itertools

def compare_histo(bluename,blue,redname,red,xticks1,xticks2,title,number_of_columns,degree_label):
    fig, ax = plt.subplots()

    index = np.arange(number_of_columns)
    bar_width = 0.35
    
    opacity = 0.8
    error_config = {'ecolor': '0.3'}
    
    rects1 = plt.bar(index, blue, bar_width,
                     alpha=opacity,
                     color='b',
                     error_kw=error_config,
                     label=bluename)
    
    rects2 = plt.bar(index + bar_width, red, bar_width,
                     alpha=opacity,
                     color='r',
                     error_kw=error_config,
                     label=redname)
    
    plt.xlabel('Features combination')
    plt.ylabel('AUC')
    plt.title(title)
    plt.xticks(xticks1,xticks2)
    
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.7))
    
    locs, labels = plt.xticks()
    plt.setp(labels, rotation=degree_label)
    
    plt.tight_layout()
    plt.ylim(0.7,1)
    plt.show()
    
def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j], horizontalalignment="center", color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

# modulo
from sklearn.metrics import confusion_matrix

# crei l'oggetto
official_clf = joblib.load('/home/denis/catkin_ws/src/mm_recon/cls/best_classifier_1_1.pkl')
cnf_matrix = confusion_matrix(final_te_dataset[:,-1], official_clf.predict(final_te_dataset[:,:-1]))

# la disegni
# se metti nermalized=false la fa con i valori reali altrimenti da 0 a 1
plt.figure()
plot_confusion_matrix(cnf_matrix, classes=["True", "False"], normalize=True, title='Normalized confusion matrix')

f_names = []
for key in sorted_plot_3sub.keys():
    string = ""
    for subk in key:
        string+=features_tool.features_used[subk].__name__+" "
    f_names.append(string)

compare_histo('Random Forest',np.array(sorted_plot_nosub.values())[:,0],
              'Decision Tree',np.array(sorted_plot_nosub.values())[:,1],
              x_nosub_dt,
              #sorted_plot_3sub.keys(),
              f_names,
              'Classifiers performances using one window',15,70)

compare_histo('Random Forest',np.array(sorted_plot_3sub.values())[:,0],
              'Decision Tree',np.array(sorted_plot_3sub.values())[:,1],
              x_nosub_dt,
              #sorted_plot_3sub.keys(),
              f_names,
              'Classifiers performances using 3 windows',15,70)

compare_histo('1 window',np.array(sorted_plot_nosub.values())[:,0],
              '3 windows',np.array(sorted_plot_3sub.values())[:,0],
              x_nosub_dt,
              #sorted_plot_3sub.keys(),
              f_names,
              'Random Forest AUCs',15,70)

compare_histo('No Sub',np.array(sorted_plot_nosub.values())[-1],
              '3 Sub',np.array(sorted_plot_3sub.values())[-1],
              [0.35,1.35],
              ['Random Forest','Decision Tree'],
              'Subindowing effectiveness',2,0)
#%%
#==============================================================================
# Color Maps
#==============================================================================
for clf in clfs:
    if hasattr(clf, 'feature_importances_'):
        feature_denisity = np.array(clf.steps[2][1].feature_importances_[:-1])
        feature_denisity = feature_denisity.reshape(
                len(features_tool.features_used),
                len(columns))
        plt.yticks(np.arange(len(features_tool.features_used)),['mean','std','range','energy'])
        plt.xticks(np.arange(len(columns)),columns)
        locs, labels = plt.xticks()
        plt.title("Features importance using 1 window")
        plt.setp(labels, rotation=90)
        plt.imshow(feature_denisity,interpolation='none',cmap = plt.get_cmap("OrRd"))
        plt.colorbar()

#%%
feature_denisity = np.array(official_clf.steps[2][1].feature_importances_[:-1])
feature_denisity = feature_denisity.reshape(
        len(features_tool.features_used)*3,
        len(columns))
plt.yticks(np.arange(len(features_tool.features_used)*3),
           ['mean 1','std 1','range 1','energy 1',
           'mean 2','std 2','range 2','energy 2',
           'mean 3','std 3','range 3','energy 3'])
plt.xticks(np.arange(len(columns)),columns)
locs, labels = plt.xticks()
plt.title("Features importance using 3 windows")
plt.setp(labels, rotation=90)
plt.imshow(feature_denisity,interpolation='none',cmap = plt.get_cmap("OrRd"))
plt.colorbar()
plt.show()
#%%
testset =final_te_dataset
y_pred_rf = official_clf.predict_proba(testset[:,:-1])[:, 1]
fpr_rf, tpr_rf, _ = metrics.roc_curve(testset[:,-1:], y_pred_rf)

plt.figure(1)
plt.plot([0, 1], [0, 1], 'k--')
plt.plot(fpr_rf, tpr_rf, label='RF')
plt.xlabel('False positive rate')
plt.ylabel('True positive rate')
plt.title('ROC curve')
plt.legend(loc='best')
plt.show()