# %%
from __future__ import absolute_import, division, print_function

import numpy as np
import rosbag_pandas
import pandas as pd

import os.path
from tqdm import tqdm

def bagfile(filename):
    return  "/home/denis/catkin_ws/src/mm_recon/bags/myo/"+filename+".bag"

tr_bags = [bagfile('money_in_the_bank'),
           bagfile('tony'),
           bagfile('pointing_to_the_ground'),
           bagfile('digna'),
           bagfile('during_work'),
           bagfile('hard_pointing'),
           bagfile('die_hard_boris')]

te_bags = [bagfile('money_in_the_bank_test'),
           bagfile('tony_test'),
           bagfile('pointing_to_the_ground_test'),
           bagfile('digna_test'),
           bagfile('hard_pointing_test')]

myo_columns = ['myo1_accel__vector_x',
           'myo1_accel__vector_y',
           'myo1_accel__vector_z',
           'myo1_rotation__quaternion_w',
           'myo1_rotation__quaternion_x',
           'myo1_rotation__quaternion_y',
           'myo1_rotation__quaternion_z']

snd_columns = ['robotsound__arg']

for window_time in [2.5]:
    sensor_frequency = 50
    w_size = int(window_time*sensor_frequency)
    
    def snds_as_tuples(snd_index):
        tuples = []
        for i in range(0,len(snd_index),2):
            up = snd_index[i]
            down = snd_index[i+1]
            tuples.append([up,down])
        return tuples
    
    def validate_TP_window(window,s1,s2,delta=0.5,x=2):
        # To be a positive window
        # Criteria 1: starts at or before s1 + delta
        if window.index[0] <= s1+pd.Timedelta(seconds=delta):
            # Criteria 2: ends at or before s2 + delta
            if window.index[len(window.index)-1] <= s2+pd.Timedelta(seconds=delta):
                # Criteria 3: ends at or after s1 + x
                if window.index[len(window.index)-1] >= s1+pd.Timedelta(seconds=x):
                    return True
        return False
    
    def validate_TN_window(window,s1,s2,delta=0.3):
        # To be a non-ambiguous false window
         # Criteria 1: starts at or after s2 - delta
        if window.index[0] >= s2-pd.Timedelta(seconds=delta):
            return True
        # Criteria 2: ends at or before s1 + delta (before the pointing movement starts)
        if window.index[len(window.index)-1] <= s1+pd.Timedelta(seconds=delta):
            return True
    
        
    def compact_dataframe(bag):
        df1acc = rosbag_pandas.bag_to_dataframe(bag,include=['/myo1/accel'])
        df1rot = rosbag_pandas.bag_to_dataframe(bag,include=['/myo1/rotation'])
        df2acc = rosbag_pandas.bag_to_dataframe(bag,include=['/myo2/accel'])
        df2rot = rosbag_pandas.bag_to_dataframe(bag,include=['/myo2/rotation'])
        
        df = df1acc
        
        for temp_df in [df1rot,df2acc,df2rot]:
            temp_df = temp_df.reindex(df1acc.index,method="nearest")
            df = pd.concat([df,temp_df], axis=1)
        
        return df
    
    def process_bag_files(bags):
        dataset = []
        ambiguous = 0
        for bag in tqdm(bags):
            
            myodf = compact_dataframe(bag)
            if bag != bagfile('during_work'):
                snddf = rosbag_pandas.bag_to_dataframe(bag,include=["/robotsound"])[snd_columns][:-1]
            else:
                snddf = pd.DataFrame()
                
            tuples = snds_as_tuples(snddf.index)
            
            for i in tqdm(np.arange(len(myodf)-w_size)):
                w_score = 0
                w = myodf[i:i+w_size]
                for s1,s2 in tuples:
                    if(w.index[0]>s2 or w.index[len(w)-1]<s1):
                        continue
                    if validate_TP_window(w,s1,s2):
                        w_score+=1
                        
                if w_score == 1:
                    dataset.append({'M':w.values,'y':1})
                else:
                    if validate_TN_window(w,s1,s2):
                        dataset.append({'M':w.values,'y':0})
                    else:
                        ambiguous+=1
                        
        df = pd.DataFrame(dataset)
        print('\nClass distribution\n')
        print(df['y'].value_counts())
        print(ambiguous)
        return df
        
    
#==============================================================================
#     
#     #%%
#==============================================================================
    
    tr_df = process_bag_files(tr_bags)
    tr_df_positives = tr_df[tr_df['y'] == 1]
    tr_df_negatives = tr_df[tr_df['y'] == 0].sample(n=len(tr_df_positives))
    tr_df_balanced = pd.concat([tr_df_positives,tr_df_negatives], axis=0)
    
    
#==============================================================================
     te_df = process_bag_files(te_bags)
#     te_df_positives = te_df[te_df['y'] == 1]
#     te_df_negatives = te_df[te_df['y'] == 0].sample(n=len(te_df_positives))
#     te_df_balanced = pd.concat([te_df_positives,te_df_negatives], axis=0)
#==============================================================================
#==============================================================================
#     
#==============================================================================
#%%    
    import PyKDL as kdl
    from math import pi
    
    def pick_samples(df,ns):
        samples=df.sample(n=ns)
        X = np.array(list(samples["M"]))
        y = np.array(list(samples["y"]))
        
        #X[:,:,[0,1,2,7,8,9]]=0
        
        return X,y
    
    def rotate_quaternion(q,alpha,beta):
        b = q / np.linalg.norm(q)
        
        original = kdl.Rotation.Quaternion(*b)
        
        rot = kdl.Rotation.Rot(kdl.Vector(0,0,1),alpha)
        
        # Try
        rot.DoRotX(beta)
        
        rotated = rot * original
        
        return rotated.GetQuaternion()
    
    def pick_altered_samples(df,ns):
        samples = df.sample(n=ns)
        
        X = np.array(list(samples["M"]))
        y = np.array(list(samples["y"]))
    
        for m in X:
            random_angle_z = np.random.uniform(0,2*pi)
            random_angle_x = np.random.uniform(-pi/4,pi/4) 
            for i in range(len(m)):
    #==============================================================================
    #             rot1 = kdl.Rotation.Quaternion(*m[i,3:7])
    #             rot2 = kdl.Rotation.Quaternion(*m[i,10:14])
    #             rot1.DoRotX(random_angle)
    #             rot2.DoRotX(random_angle)
    #             m[i,3:7] = np.array(rot1.GetQuaternion())
    #             m[i,10:14] = np.array(rot2.GetQuaternion())
    #==============================================================================
                q1 = m[i,3:7]
                q2 = m[i,10:14]
                m[i,3:7] = np.array(rotate_quaternion(q1,random_angle_z,random_angle_x))
                m[i,10:14] = np.array(rotate_quaternion(q2,random_angle_z,random_angle_x))
                
    
        #X[:,:,[0,1,2,7,8,9]]=0
                
        return X,y
    
    def tr_generator(df,batch_size):
        while True:
            yield(pick_altered_samples(df,batch_size))
    
    def te_generator(df,batch_size):
        while True:
            yield(pick_samples(df,batch_size))
    
    import keras
    from keras.models import Sequential
    from keras.layers import Activation, Flatten, Dense
    from sklearn import metrics
    
    #==============================================================================
    # model = Sequential()
    # model.add(Flatten(input_shape=(w_size,14)))
    # model.add(Dense(3))
    # model.add(Activation('relu'))
    # model.add(Dense(1))
    # model.add(Activation('sigmoid'))
    # model.compile(loss='binary_crossentropy',
    #               optimizer='rmsprop',
    #               metrics=['accuracy'])
    # 
    #==============================================================================
    model = keras.models.Sequential()
    model.add(keras.layers.Conv1D(filters=5, kernel_size=10,input_shape=(w_size,14)))
    model.add(keras.layers.Activation('relu'))
    model.add(keras.layers.MaxPooling1D(pool_size=2, padding='valid'))
    model.add(keras.layers.Flatten())
    model.add(keras.layers.Dense(32))
    model.add(keras.layers.Activation('relu'))
    model.add(keras.layers.Dense(1))
    model.add(keras.layers.Activation('sigmoid'))
    model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])
    model.count_params()
#==============================================================================
#     #%%
#==============================================================================
    import pathlib2
    import time
    
    batch_size = 128
    epochs = 25
    (X_val,y_val)=next(te_generator(te_df,5000))
    
    (pathlib2.Path(".")/"logs").mkdir(exist_ok=True)
    
    history = model.fit_generator(
            te_generator(tr_df,batch_size),
            steps_per_epoch = 100,
            epochs = epochs,
            validation_data=(X_val,y_val),
            callbacks=[keras.callbacks.TensorBoard(log_dir='./logs/'+time.strftime("no_data_aug"), 
                                                   histogram_freq=0, 
                                                   write_graph=False, 
                                                   write_images=False)]
            )
    
    
#==============================================================================
#     #%%
#==============================================================================
    from keras.utils import plot_model
    
    # plot_model(model, to_file='model.png')
    #print("Model structure saved")
    keras.models.save_model(model,time.strftime("no_data_aug_")+"pointing.model")
#%% 
#==============================================================================
#     #%%
#==============================================================================
    from sklearn import metrics
    from sklearn.metrics import accuracy_score
    
    X_te = []
    y_te = []
    
    for m,y  in tqdm(te_df_balanced.values):
        X_te.append(m)
        y_te.append(y)
    
    X_te = np.array(X_te)
    y_te = np.array(y_te)
    
    y_score = model.predict_proba(X_te)
    y_pred = y_score>0.5
    accuracy = accuracy_score(y_te,y_pred)
    fpr, tpr, _ = metrics.roc_curve(y_te, y_score)
    area_under_curve = metrics.roc_auc_score(y_te, y_score)
    
#==============================================================================
#     #%%
#     
#     
#     
#     def print_samples(df):
#         for m,y in df:
#             print("Class:"+str(y))
#             plt.pcolor((m-np.mean(m,axis=0))/np.std(m,axis=0),cmap="viridis")
#             plt.show()
#             
#     print_samples(pick_altered_samples(tr_df_positives,20))        
#==============================================================================
#==============================================================================
# #%%       
#==============================================================================
    # Plot auc
    import matplotlib.pyplot as plt
    y = []
    y_hat = []
    for i in tqdm(xrange(1000)):
        im = next(te_generator(te_df,batch_size))
        y.append(int(im[1][0]))
        y_hat.append(model.predict(im[0])[0,0])
        
    fpr, tpr, _ = metrics.roc_curve(y, y_hat)
    auc = metrics.auc(fpr, tpr)
    
    plt.figure()
    plt.plot([0, 1], [0, 1], 'k--')
    plt.plot(fpr, tpr, label='RF')
    plt.xlabel('False positive rate')
    plt.ylabel('True positive rate')
    plt.title('ROC curve'+str(window_time))
    plt.legend(loc='best')
    plt.draw()