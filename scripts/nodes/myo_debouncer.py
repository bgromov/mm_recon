#!/usr/bin/env python
import rospy
from std_msgs.msg import Float32
from geometry_msgs.msg import Twist

debouncer = 0
toggle = False

#==============================================================================
# def translate(data):
#     global toggle
#     
#     twist = Twist()
#     if data.data==1.0:
#         if toggle:
#             twist.linear.x = 0.0
#         else:
#             twist.linear.x = 1.0
#         toggle = not(toggle)
#         rospy.loginfo("STOP")
#         
#     return twist
#==============================================================================
        
def callback(data,pubs):
    global debouncer

    if debouncer <= 0 and data.data > rospy.get_param('~threshold'): #threshold
        rospy.loginfo(rospy.get_caller_id() + "Proba: %.3f", data.data)
        # pubs[0].publish(translate(data))
        pubs[1].publish(1.0)
        debouncer = rospy.get_param('~debounce_times')

    debouncer-=1
        
    
        
def listener(pubs):
    rospy.init_node('myo_debouncer', anonymous=True)
    rospy.Subscriber(str(rospy.get_param('~topic_to_debounce')),Float32,callback,pubs)
    rospy.spin()

if __name__ == '__main__':
    pubs = [rospy.Publisher('/cmd_vel', Twist, queue_size=1), #whatever topic we need to interact with robots
            rospy.Publisher('/pointing_debounced', Float32, queue_size=1)]
    listener(pubs)
    