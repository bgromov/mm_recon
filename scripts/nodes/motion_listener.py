#!/usr/bin/env python
#%%
from __future__ import print_function

import rospy, os, sys
from sensor_msgs.msg import Imu
from sklearn.externals import joblib
from sklearn import tree
import numpy as np
import pandas as pd
import sys

clf = joblib.load('/home/denis/filename.pkl')
window=pd.DataFrame([])
window_size=200
#%%
def callback(data):
    print("%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t" % (
        data.orientation.x,data.orientation.y,data.orientation.z,
        data.angular_velocity.x,data.angular_velocity.y,data.angular_velocity.z,
        data.linear_acceleration.x, data.linear_acceleration.y, data.linear_acceleration.z
    ))

def listener():
    global imu_pub
    rospy.init_node('imu_listener', anonymous=True)
    rospy.Subscriber('/imu/data', Imu, callback)
    rospy.spin()

if __name__ == '__main__':
    listener()
