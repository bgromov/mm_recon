# %%
from __future__ import absolute_import, division, print_function

import pylab
import imageio
import matplotlib.pyplot as plt
import os

filenames = [['scripts/video/poinsomfing.mp4',"/home/denis/Videos/frames/point/{:05d}.png"],
              ['scripts/video/BIP.mp4',"/home/denis/Videos/frames/stop/{:05d}.png"]]
for filename in filenames:
    print(filename)
    vid = imageio.get_reader(filename[0],  'ffmpeg')
    length = vid.get_length()-15
    nums = [i for i in xrange(0,length,length//5)]
    for num in nums:
        fig,ax = plt.subplots(nrows=1,figsize=(12,10))
        image = vid.get_data(num)
        ax.imshow(image)
        ax.set_axis_off()
        fig.tight_layout()
        fig.savefig(filename[1].format(num))
        
# Import Pillow:
from PIL import Image

# Load the original image:
#img = Image.open("flowers.jpg")
square = 600
deltah = 100
for path in ['/home/denis/Videos/frames/stop','/home/denis/Videos/frames/point']:
    for filename in os.listdir(path):
        if filename.endswith(".png"):
            print(os.path.join(path, filename))
            img = Image.open(os.path.join(path, filename))
            half_the_width = img.size[0] / 2
            half_the_height = img.size[1] / 2 - deltah
            img4 = img.crop(
                (
                    half_the_width - square//2,
                    half_the_height - square//2,
                    half_the_width + square//2,
                    half_the_height + square//2
                )
            )
            img4.save(os.path.join(path, filename))
            continue
        else:
            continue