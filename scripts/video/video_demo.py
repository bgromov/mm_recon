# %%
from __future__ import absolute_import, division, print_function

import imageio
import matplotlib.pyplot as plt
import sys
sys.path.append('/home/denis/catkin_ws/src/mm_recon/src/mm_recon')
import load_video_demo_plot as lvdp
import numpy as np
from tqdm import tqdm
import pandas as pd

filename = '/home/denis/Videos/myo/GOPR2600.MP4'
vid = imageio.get_reader(filename,'ffmpeg')

#frames = np.arange(100,200,1)
frames = np.arange(180,280)
if len(sys.argv) == 4:
    frames = np.arange(int(sys.argv[1]),
                       int(sys.argv[1])+int(sys.argv[2]),
                       1)
else:
    print('sys params not found')

for f in tqdm(frames):
    plot_index = (f / 30) - float(sys.argv[3]) # first sound occurs at 6th second
    image = vid.get_data(f)
    fig,(ax1,ax2) = plt.subplots(nrows=2,figsize=(12,10),gridspec_kw={'height_ratios':[1,0.5]})
    #==============================================================================
    #         
    #==============================================================================
    for treshold,thits in lvdp.hits.items():
    
        tp = []
        fp = []
        sounds_missed = pd.DataFrame(lvdp.sounds)
        for hit in thits:
            if hit <= plot_index:
                df = lvdp.sounds[hit-3:hit]
                if len(df) > 0:
                    tp.append(hit)
                    sounds_missed = sounds_missed.drop(df.index.values)
                else:
                    fp.append(hit)
               
        tp_points = ax2.plot(pd.core.indexes.numeric.Float64Index(tp),
                 [float(treshold)]*len(tp),
                 'go')
        fp_points = ax2.plot(pd.core.indexes.numeric.Float64Index(fp),
                 [float(treshold)]*len(fp),
                 'rx')
        fn_points = ax2.plot(pd.Series(sounds_missed.index[:-1]),
                 [float(treshold)]*(len(sounds_missed)-1),
                 'bo',mfc='none',mec='b')
        ax2.axhline(y=treshold,c="blue",linewidth=0.5,alpha=0.5,label='Threshold')
        
    
    ax2.plot([-10],'go',label='True Positive')
    #ax2.plot([-10],'rx',label='False Positive')
    #ax2.plot([-10],'bo',label='False Negative',mfc='none',mec='b')
    
    idx = pd.Series(lvdp.sounds.index[:-1])
    ax2.vlines(x=idx, ymin=0, ymax=1,linewidth=5, alpha=0.8, color="g",label='Sound')
    ax2.vlines(x=plot_index, ymin=0, ymax=1,linewidth=5, alpha=0.2, color="black",label='Now')
    ax2.plot(lvdp.gestures_proba[:plot_index],label='Pointing Probability')
    
    ax2.legend(ncol=6,loc='upper center', bbox_to_anchor=(0.5,1.15))
    
    ax2.set_xlim([plot_index-3,plot_index+3])
    ax2.set_ylim([0,1])
    
    ax1.imshow(image)
    ax1.set_axis_off()
    fig.tight_layout()
    if lvdp.gestures_proba[plot_index-2:plot_index].max().item() > 0.5:
        fig.savefig("/home/denis/Videos/myo/video_demo/{:05d}.png".format(f),facecolor='#00994C')
    else:
        fig.savefig("/home/denis/Videos/myo/video_demo/{:05d}.png".format(f))

    fig.clf()   # Clears the current figure